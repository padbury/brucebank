import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { SearchData } from './components/SearchData';
import { CreateAccount } from './components/CreateAccount';
import { ViewAccount } from './components/ViewAccount';

import './custom.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/create-account' component={CreateAccount} />
                <Route path='/search' component={SearchData} />
                <Route path='/view-account/:id' component={ViewAccount} />
            </Layout>
        );
    }
}
