import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export class SearchData extends Component {
    static displayName = SearchData.name;

    constructor(props) {
        super(props);
        this.state = { accounts: [], q: '', loading: false };
    }

    formChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({ [nam]: val });
        if (event.target.value.length > 2) {
            this.doSearch();
        }
    }

    doSearch() {
        this.setState({ loading: true });
        this.setState({ accounts: [] });
        axios.get(`/account/find?q=${this.state.q}`)
            .then((res) => {
                this.setState({ accounts: res.data.results });
                this.setState({ loading: false });
            })
            .catch((err) => {
                console.log(err);

            });
    }

    componentDidMount() {
        const { match: { params } } = this.props;

        console.log(params.id);
       
    }

    static renderTable(items) {
        return (
            items.length > 0 &&
                <table className='table table-striped' aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Postcode</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {items.map(item =>
                            <tr key={item.id}>
                                <td>{item.firstname}</td>
                                <td>{item.lastname}</td>
                                <td>{item.address.postcode}</td>
                                <td>
                                    <Link to={`/view-account/${item.id}`}>View</Link>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
                
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : SearchData.renderTable(this.state.accounts);

        return (
            <div>
                <h1 id="tabelLabel" >Search Accounts</h1>
                <p>Please enter your search below</p>
                <input type="text" name="q" onChange={this.formChangeHandler} placeholder="Search accounts" className="form-control" />
                <small>Type more than 2 characters to begin a search. This field searchs on the name, address line 1, town and postcode</small>
                {contents}
            </div>
        );
    }
}
