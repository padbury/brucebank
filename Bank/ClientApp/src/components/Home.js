import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <h1>Merry Christmas</h1>
                <p>A Down and Dirty Transaction Simulator. Built with:</p>
                <ul>
                    <li>React JS</li>
                    <li>.Net Core 3.1</li>
                    <li>SQL Server 2017</li>
                    <li>Custom Data Repository Layer using my own Repository Pattern and NPoco</li>
                    <li>getAddresses.io (limited to 20 lookups per day)</li>
                    <li>exchangeratesapi.io for currency conversions</li>
                </ul>
                <p>This was quite a lot of work and I don't think it's feasible to complete it all in the 4-6 hour target so there's quite a bit missing. Basically it lacks polish, it lacks a test project and some code comments and probably has bugs. But it'll give you
                    an idea of where my skillset is at and how I code. I've tried to add comments and explain my thinking and why I've done things in the way I have.</p>
                <p>In regards to account numbers, I've used a stripped GUID for this as it's way beyond scope to make an account number generator and I'm not a fan of using an autoincremented number as
                    I'd also need to wrap the whole thing in a "lock" block to ensure that the same ID isn't created twice. Guid was easier :)</p>
                <p><Link to={`/create-account`}>Create an account to get started</Link></p>
            </div>
        );
    }
}
