import React, { Component } from 'react';
import axios from 'axios';

export class ViewAccount extends Component {
    static displayName = ViewAccount.name;

    constructor(props) {
        super(props);
        this.state = {
            errors: [],
            allAccounts: [],
            allTransactions:[],
            loading: false,
            AccountId: '',
            addFunds: {
                exchangeRate: 1, currencyCode: 'GBP', amount: 0, errors: []
            },
            transferFunds: {
                exchangeRate: 1, currencyCode: 'GBP', amount: 0
            },
            account: {},
            balance: {}
        };
    }




    componentDidMount() {
        const { match: { params } } = this.props;
        this.setState({ AccountId: params.id });
        this.getAccount(params.id);
        this.getAllAccounts(params.id);
        this.getTransactions(params.id);
    }

    // Fetch the account details from the server
    getAccount(id) {
        axios.get(`/account/get?id=${id}`)
            .then((res) => {
                this.setState({ account: res.data.account });
                this.setState({ balance: res.data.balance });
                this.setState({ addFunds: { currencyCode: res.data.account.currency.code, exchangeRate: 1, amount: 0 } });
                this.setState({ transferFunds: { currencyCode: res.data.account.currency.code, exchangeRate: 1, amount: 0, destinationAccountId: '' } });
                this.setState({ errors: [] });
            })
            .catch((err) => {
                console.log(err);
                this.setState({ errors: err.response.data.errors });
            });
    }

    // Get all accounts from the server to bind to the dropdown for transfers
    getAllAccounts(accountId) {
        axios.get('/account/getall')
            .then((res) => {
                // filter accounts so that we don't get the option to transfer money to ourselves
                var accounts = res.data.accounts.filter(function (o) {
                    return o.id != accountId;
                });
                this.setState({ allAccounts: accounts });

            })
            .catch((err) => {
                console.log(err);
                this.setState({ errors: err.response.data.errors });
            });
    }

    // Get all accounts from the server to bind to the dropdown for transfers
    getTransactions(id) {
        axios.get(`/account/gettransactions?id=${id}`)
            .then((res) => {
                this.setState({ allTransactions: res.data.transactions });
            })
            .catch((err) => {
                console.log(err);
                this.setState({ errors: err.response.data.errors });
            });
    }

    getExchangeRate(baseCurrency, code, propName, prop) {
        axios.get(`https://api.exchangeratesapi.io/latest?base=${code}&symbols=${baseCurrency}`)
            .then((res) => {
                var exRate = 1;
                switch (baseCurrency) {
                    case "GBP":
                        exRate = parseFloat(res.data.rates.GBP);
                        break;
                    case "EUR":
                        exRate = parseFloat(res.data.rates.EUR);
                        break;
                    case "USD":
                        exRate = parseFloat(res.data.rates.USD);
                        break;
                }
                prop.exchangeRate = exRate;
                this.setState({ [propName]: prop })
            })
            .catch((err) => {
                console.log(err);
                this.setState({ errors: err.response.data.errors });
            });
    }



    formAddFundsCurrencyChangeHandler = (event) => {
        var prop = { ...this.state.addFunds }
        prop.currencyCode = event.target.value;
        // if the currency to be used is different to the account currency, we will be applying a conversion
        if (event.target.value != this.state.account.currency.code) {
            this.getExchangeRate(this.state.account.currency.code, event.target.value, 'addFunds', prop);
        } else {
            prop.exchangeRate = 1;
            this.setState({ addFunds: prop })
        }
    }

    formAddFundsAmountChangeHandler = (event) => {
        var prop = { ...this.state.addFunds }
        prop.amount = parseFloat(event.target.value);
        this.setState({ addFunds: prop })
        console.log(prop)
    }

    formAddFundsSubmitHandler = (event) => {
        event.preventDefault();
        axios.post('/account/addfunds', { currencyCode: this.state.addFunds.currencyCode, amount: this.state.addFunds.amount, accountId: this.state.account.id })
            .then((res) => {

                // update balance
                var prop = { ...this.state.balance }
                prop.formatted = res.data.balance.formatted;
                this.setState({ balance: prop });

                // Update Transactions
                this.getTransactions(this.state.account.id);

                // reset funds in form
                var propFunds = { ...this.state.addFunds };
                propFunds.amount = 0;
                propFunds.errors = [];
                this.setState({ addFunds: propFunds });

                // notify user
                alert('Cheap mans notification to say that funds have been added.')
            })
            .catch((err) => {
                console.log(err);
                // catch and display any errors
                var prop = { ...this.state.addFunds };
                prop.errors = err.response.data.errors;
                this.setState({ addFunds: prop });
            });
    }

    // Transfer Form Methods - I'm aware that in a proper application these forms would be pulled into their own components rather than be dumped here but time 
    // won't permit me to do that.



    // Change currency, update exchange rate
    formTransferFundsCurrencyChangeHandler = (event) => {
        var prop = { ...this.state.transferFunds }
        prop.currencyCode = event.target.value;
        prop.errors = [];
        // if the currency to be used is different to the account currency, we will be applying a conversion
        if (event.target.value != this.state.account.currency.code) {
            this.getExchangeRate(this.state.account.currency.code, event.target.value, 'transferFunds', prop);
        } else {
            prop.exchangeRate = 1;
            this.setState({ transferFunds: prop })
        }
    }

    // Change funds
    formTransferFundsAmountChangeHandler = (event) => {
        var prop = { ...this.state.transferFunds }
        prop.amount = parseFloat(event.target.value);
        prop.errors = [];
        this.setState({ transferFunds: prop })
        console.log(prop)
    }

    // Set destination account
    formTransferFundsDestinationChangeHandler = (event) => {
        var prop = { ...this.state.transferFunds }
        prop.destinationAccountId = event.target.value;
        this.setState({ transferFunds: prop })
    }

    // Submit to server
    formTransferFundsSubmitHandler = (event) => {
        event.preventDefault();
        axios.post('/account/transferfunds', { currencyCode: this.state.transferFunds.currencyCode, amount: this.state.transferFunds.amount, sourceAccount: this.state.account.id, destinationAccount: this.state.transferFunds.destinationAccountId })
            .then((res) => {

                // Update Balance
                var prop = { ...this.state.balance }
                prop.formatted = res.data.balance.formatted;
                this.setState({ balance: prop });

                  // Update Transactions
                this.getTransactions(this.state.account.id);

                // reset funds in form
                var propTrans = { ...this.state.transferFunds };
                propTrans.amount = 0;
                propTrans.errors = [];
                this.setState({ transferFunds: propTrans });

 
                // notify user
                alert('Cheap mans notification to say that funds have been transfered.')
            })
            .catch((err) => {
                console.log(err);
                var prop = { ...this.state.transferFunds };
                prop.errors = err.response.data.errors;
                this.setState({ transferFunds: prop });
            });
    }

    static renderTable(items) {
        return (
            items.length > 0 &&
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {items.map(item =>
                        <tr key={item.id}>
                            <td>{new Date(item.created).toLocaleString('en-GB')}</td>
                            <td>{item.amount.formatted}</td>
                        </tr>
                    )}
                </tbody>
            </table>

        );
    }

    render() {
        return (
            <div>
                { (this.state.errors != undefined && this.state.errors.length > 0) &&
                    <ul className="alert alert-danger">{this.state.errors.map((err) => <li key={err}> {err}</li>)} </ul>}

                { this.state.errors.length == 0 && this.state.account.id !== undefined &&
                    <div>
                        <h1>Account: {this.state.account.id}</h1>
                        <h3>Balance: {this.state.balance.formatted}</h3>
                        <dl>
                            <dt>Account Base Currency</dt>
                            <dd>{this.state.account.currency.code}</dd>
                            <dt>Account Type</dt>
                            <dd>{this.state.account.accountType}</dd>
                            <dt>Account Holder Name</dt>
                            <dd>{this.state.account.firstname} {this.state.account.lastname}</dd>
                            <dt>Account Holder Address</dt>
                            <dd>{this.state.account.address.formatted}</dd>
                        </dl>
                        <hr />

                        <h4>Add Funds</h4>
                        <form onSubmit={this.formAddFundsSubmitHandler} className="form form-horizontal">
                            <div className="row">
                                <div className="col-md-2">
                                    <div className="form-group">
                                        <select name='AddFundsCurrencyCode' onChange={this.formAddFundsCurrencyChangeHandler} value={this.state.addFunds.currencyCode} className="form-control">
                                            <option value="GBP">&pound;</option>
                                            <option value="EUR">&euro;</option>
                                            <option value="USD">$</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">  <div className="form-group">
                                    <input
                                        type='number'
                                        name='Amount'
                                        className="form-control"
                                        placeholder="Amount"
                                        value={this.state.addFunds.amount}
                                        onChange={this.formAddFundsAmountChangeHandler}
                                    />
                                    {this.state.addFunds.exchangeRate != 1 &&
                                        <small>The current exchange rate is {this.state.addFunds.exchangeRate} and you will be depositing {(this.state.addFunds.exchangeRate * this.state.addFunds.amount).toFixed(2)}</small>
                                    }
                                </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                <input className="btn btn-primary" type='submit' value="Add Funds" />

                                {(this.state.addFunds.errors !== undefined && this.state.addFunds.errors.length > 0) &&
                                    <ul className="alert alert-danger">{this.state.addFunds.errors.map((err) => <li key={err}> {err}</li>)} </ul>}

                                </div>
                            </div>
                        </form>
                        <hr />

                        <h4>Transfer Funds</h4>
                        <form onSubmit={this.formTransferFundsSubmitHandler} className="form form-horizontal">
                            <div className="row">
                                <div className="col-md-2">
                                    <div className="form-group">
                                        <select name='TransferFundsCurrencyCode' onChange={this.formTransferFundsCurrencyChangeHandler} value={this.state.transferFunds.currencyCode} className="form-control">
                                            <option value="GBP">&pound;</option>
                                            <option value="EUR">&euro;</option>
                                            <option value="USD">$</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <input
                                            type='number'
                                            name='Amount'
                                            className="form-control"
                                            placeholder="Amount"
                                            value={this.state.transferFunds.amount}
                                            onChange={this.formTransferFundsAmountChangeHandler}
                                        />
                                        {this.state.transferFunds.exchangeRate != 1 &&
                                            <small>The current exchange rate is {this.state.transferFunds.exchangeRate} and you will be transfering {(this.state.transferFunds.exchangeRate * this.state.transferFunds.amount).toFixed(2)}</small>
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label>to account</label>
                                        <select name='TransferTo' onChange={this.formTransferFundsDestinationChangeHandler} className="form-control">
                                            <option value="">Please select an account</option>
                                            {this.state.allAccounts.map((a) => <option key={a.id} value={a.id}>{a.name} ({a.id})</option>)}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <input className="btn btn-primary" type='submit' value="Transfer Funds" />

                                    {((this.state.transferFunds !== undefined && this.state.transferFunds.exchangeRate * this.state.transferFunds.amount).toFixed(2) > this.state.balance.amount) &&
                                    <div className="alert alert-danger">You do not have enough funds for this transfer</div>}

                                    {(this.state.transferFunds.errors !== undefined && this.state.transferFunds.errors.length > 0) &&
                                    <ul className="alert alert-danger">{this.state.transferFunds.errors.map((err) => <li key={err}> {err}</li>)} </ul>}
                                </div>
                            </div>
                        </form>

                        <hr />
                        <h4>Transaction History</h4>
                        {ViewAccount.renderTable(this.state.allTransactions)}
                    </div>
                }
            </div>
        );
    }
}
