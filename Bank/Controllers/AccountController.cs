﻿using Bank.Core.Models;
using Bank.Core.Services.Interfaces;
using Bank.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Bank.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        private readonly ILogger<AccountController> _logger;
        private readonly ICustomerAccountService _customerAccountService;

        public AccountController(ILogger<AccountController> logger, ICustomerAccountService customerAccountService)
        {
            _logger = logger;
            _customerAccountService = customerAccountService;
        }

        [HttpGet, Route("get")]
        public IActionResult Get(string id)
        {
            var account = _customerAccountService.GetById(id);
            if (account == null)
                return BadRequest(new { Errors = new string[] { "Account Does Not Exist" } });

            var balance = _customerAccountService.GetAccountBalance(account.Id);

            return Ok(new { account = account, balance = balance });
        }

        [HttpPost, Route("add")]
        public IActionResult Add(CustomerAccountRequest request)
        {
            var newAccount = new CustomerAccount
            {
                SortCode = _customerAccountService.GetSortCode(request.CurrencyCode),
                Firstname = request.Firstname,
                Lastname = request.Lastname,
                Currency = new Currency { Code = request.CurrencyCode },
                Address = new Address
                {
                    AddressLine1 = request.AddressLine1,
                    AddressLine2 = request.AddressLine2,
                    Town = request.Town,
                    Postcode = request.Postcode,
                    County = request.County,
                    Country = request.Country
                },
                AccountType = request.AccountType
            };

            var validation = newAccount.Validate();

            if (!validation.IsValid)
                return BadRequest(new { Message = "Please correct the following errors", Errors = validation.Errors.Select(x => x.ErrorMessage) });

            if (_customerAccountService.AccountExists(newAccount))
                return BadRequest(new { Message = "Account Exists", Errors = new string[] { "Account Exists" } });

            try
            {
                var id = _customerAccountService.AddAccount(newAccount);

                if (request.Amount > 0)
                {
                    // Since we are creating a new account, the initial deposit currency will be the same as the holding account currency code so 
                    // no need to worry about exchange rates.
                    _customerAccountService.AddFunds(id, new Money(request.Amount, request.CurrencyCode));

                }

                return Ok(new { Id = id });

            }
            catch (Exception exc)
            {
                return BadRequest(new { Message = exc.Message, });
            }
        }


        [HttpGet, Route("getall")]
        public IActionResult GetAll()
        {


            var accounts = _customerAccountService.GetAllAccounts(1, 1000); // Hardcoded pagination simply because it's not required for a simply demo but allows this code to be extended in the future

            return Ok(new { accounts = accounts.Select(x => new { Id = x.Id, Name = x.Firstname + " " + x.Lastname }) });
        }




        [HttpGet, Route("find")]
        public IActionResult Find(string q, long p = 1, int s = 10)
        {
            try
            {
                var res = _customerAccountService.Find(q, p, s);
                return Ok(new { Results = res });

            }
            catch (Exception exc)
            {
                return BadRequest(new { Message = exc.Message, });
            }
        }

        [HttpPost, Route("transferfunds")]
        public IActionResult Transfer(MoneyTransferRequest request)
        {
            var account = _customerAccountService.GetById(request.SourceAccount);
            if (account == null)
                return BadRequest(new { Errors = new string[] { "Source Account Does Not Exists" } });

            var destinationAccount = _customerAccountService.GetById(request.DestinationAccount);
            if (destinationAccount == null)
                return BadRequest(new { Errors = new string[] { "Destination Account Does Not Exists" } });

            if (request.Amount <= 0)
                return BadRequest(new { Errors = new string[] { "Please enter an amount above zero" } });

            decimal exRate = 1;
            if (request.CurrencyCode != account.Currency.Code)
            {
                // Perform exchange rate calculation
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString($"https://api.exchangeratesapi.io/latest?base={request.CurrencyCode}&symbols={account.Currency.Code}");
                    dynamic res = Newtonsoft.Json.JsonConvert.DeserializeObject(json);

                    switch (account.Currency.Code)
                    {
                        case "GBP":
                            exRate = Convert.ToDecimal(res.rates.GBP);
                            break;
                        case "EUR":
                            exRate = Convert.ToDecimal(res.rates.EUR);
                            break;
                        case "USD":
                            exRate = Convert.ToDecimal(res.rates.USD);
                            break;
                    }
                }
            }

            // Check that the user has enough funds for the transfer
            var currentBalance = _customerAccountService.GetAccountBalance(account.Id);
            if ((request.Amount* exRate) > currentBalance.Amount)
                return BadRequest(new { Errors = new string[] { "Insufficient funds" } });

            var transfer = new MoneyTransfer
            {
                AmountToTransfer = new Money(request.Amount, request.CurrencyCode),
                DestinationAccount = request.DestinationAccount,
                SourceAccount = request.SourceAccount,
                ExchangeRateSource = 1,
                ExchangeRateDestination = exRate
            };

            try
            {
                _customerAccountService.TransferMoney(transfer);
            }
            catch (Exception exc)
            {
                return BadRequest(new { Errors = new string[] { exc.Message } });
            }



            var balance = _customerAccountService.GetAccountBalance(account.Id);
            return Ok(new { balance = balance });


            return BadRequest(new { Message = "" });
        }

        [HttpPost, Route("addfunds")]
        public IActionResult AddFunds(AddFundsRequest request)
        {
            var account = _customerAccountService.GetById(request.AccountId);
            if (account == null)
                return BadRequest(new { Errors = new string[] { "Account Does Not Exists" } });

            if (request.Amount <= 0)
                return BadRequest(new { Errors = new string[] { "Please enter an amount above zero" } });

            decimal exRate = 1;
            if (request.CurrencyCode != account.Currency.Code)
            {
                // Perform exchange rate calculation
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString($"https://api.exchangeratesapi.io/latest?base={request.CurrencyCode}&symbols={account.Currency.Code}");
                    dynamic res = Newtonsoft.Json.JsonConvert.DeserializeObject(json);

                    switch (account.Currency.Code)
                    {
                        case "GBP":
                            exRate = Convert.ToDecimal(res.rates.GBP);
                            break;
                        case "EUR":
                            exRate = Convert.ToDecimal(res.rates.EUR);
                            break;
                        case "USD":
                            exRate = Convert.ToDecimal(res.rates.USD);
                            break;
                    }
                }

            }


            _customerAccountService.AddFunds(account.Id, new Money(request.Amount, request.CurrencyCode), exRate);

            var balance = _customerAccountService.GetAccountBalance(account.Id);

            return Ok(new { balance = balance });

        }


        [HttpGet, Route("gettransactions")]
        public IActionResult GetTransactions(string id)
        {
            var account = _customerAccountService.GetById(id);
            if (account == null)
                return BadRequest(new { Errors = new string[] { "Account Does Not Exists" } });

            var transactions = _customerAccountService.GetAllTransactions(id).OrderByDescending(x=>x.Created);
  
            return Ok(new { transactions = transactions });
        }
    }
}
