﻿using NPoco;

namespace Bank.Core.Persistence.Models
{
    [PrimaryKey("Id", AutoIncrement = false)]
    public class CustomerAccount
    {
        public string Id { get; set; }

        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }

        public string CurrencyCode { get; set; }

        public string AccountType { get; set; }
    }
}
