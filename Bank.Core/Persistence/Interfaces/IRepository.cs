﻿namespace Bank.Core.Persistence.Interfaces
{
    public interface IRepository
    {
        void Submit();
    }

    public interface IRepository<T> : IRepository where T : class  {

        T GetById(object id);
    }

}
