﻿using Bank.Core.Persistence.Interfaces;
using Bank.Core.Persistence.Models;
using System.Collections.Generic;

namespace Bank.Core.Persistence.Repositories
{
    public interface IAccountTransactionRepository : IRepository<AccountTransaction>
    {
        List<AccountTransaction> GetTransactionsByAccountId(string accountId);
    }

    /// <summary>
    /// Repo for Account Transactions. I'm extending the base repo and adding to it with a specific method for this type of object
    /// </summary>
    public class AccountTransactionRepository : BaseSqlRepository<AccountTransaction>, IAccountTransactionRepository
    {
        public AccountTransactionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public List<AccountTransaction> GetTransactionsByAccountId(string accountId)
        {
            return _db.Fetch<AccountTransaction>(" WHERE AccountId = @0", accountId);
        }
    }
}
