﻿using Bank.Core.Persistence.Interfaces;
using Bank.Core.Persistence.Models;
using NPoco;
using System;

namespace Bank.Core.Persistence.Repositories
{
    public interface ICustomerAccountRepository : IRepository<CustomerAccount>
    {
        bool Exists(Guid accountId);
        Page<CustomerAccount> GetAll(long pageIndex, int pageSize);
        Page<CustomerAccount> Find(string keywords, long pageIndex, int pageSize);
    }

    /// <summary>
    /// Repo for Customer Accounts. I'm extending the base repo and adding to it with a specific methods for this type of object
    /// </summary>
    public class CustomerAccountRepository : BaseSqlRepository<CustomerAccount>, ICustomerAccountRepository
    {
        public CustomerAccountRepository(IUnitOfWork unitOfWork) : base(unitOfWork) { }

        public bool Exists(Guid accountId)
        {
            return _db.Exists<CustomerAccount>(accountId);
        }

        public bool Exists(string firstname, string lastname, string addressLine1, string postcode)
        {
            return _db.SingleOrDefault<CustomerAccount>($"SELECT Id FROM {_tableInfo.TableName} WHERE Firstname = '{firstname}' AND Lastname = '{lastname}' AND AddressLine1 =  '{addressLine1}' AND Postcode = '{postcode}'") != null;
        }

        public Page<CustomerAccount> GetAll(long pageIndex, int pageSize)
        {
            return _db.Page<CustomerAccount>(pageIndex, pageSize, $"SELECT * FROM {_tableInfo.TableName}");
        }

        public Page<CustomerAccount> Find(string keywords, long pageIndex, int pageSize)
        {
            return _db.Page<CustomerAccount>(pageIndex, pageSize, $"SELECT * FROM {_tableInfo.TableName} WHERE Postcode LIKE '%{keywords}%' OR FirstName LIKE '%{keywords}%' OR LastName LIKE '%{keywords}%' OR AddressLine1 LIKE '%{keywords}%' OR Town LIKE '%{keywords}%' ");
       
        }
    }
}
