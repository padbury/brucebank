﻿using Bank.Core.Persistence.Interfaces;
using NPoco;
using System.Collections.Generic;

namespace Bank.Core.Persistence.Repositories
{
    /// <summary>
    /// Generic Repository. If access to a particular table is standard then we can use this class directly to access it.Otherwise we can use it to extend from (which is what I do in this project)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseSqlRepository<T> : IRepository<T> where T : class
    {
        protected readonly IDatabase _db;
        protected readonly TableInfo _tableInfo;
        public BaseSqlRepository(IUnitOfWork unitOfWork)
        {
            unitOfWork.Register(this); // register this repo with the Unit Of Work
            _db = unitOfWork.Db;
            _tableInfo = TableInfo.FromPoco(typeof(T));
        }

        public void Submit()
        {
        }

        public T GetById(object id)
        {
            return _db.SingleOrDefault<T>($"select * from {_tableInfo.TableName} where id = '{id}'");
        }

        public List<T> GetAll()
        {
            return _db.Fetch<T>($"select * from {_tableInfo.TableName}");
        }

        public void Delete(object id)
        {
            _db.Delete<T>(id);
        }
        public object Insert(T entity)
        {
            return _db.Insert(entity);
        }
        public void Update(T entity)
        {
            _db.Update(entity);
        }
    }
}
