﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Validation
{

        /// <summary>
        /// Ensures the entity will have a validate method so that it can be checked for consistancy.
        /// </summary>
        public interface IValidatedEntity
        {
            ValidationResult Validate();
        }
    }

