﻿using Bank.Core.Models;
using FluentValidation;

namespace Bank.Core.Validation
{
    /// <summary>
    /// Apply server side validation rules. Very basic, not really applying full rules here.
    /// </summary>
    public class CustomerAccountValidator : AbstractValidator<CustomerAccount>
    {
        public CustomerAccountValidator()
        {
            RuleFor(reg => reg.Firstname).NotEmpty();
            RuleFor(reg => reg.Lastname).NotEmpty();
            RuleFor(reg => reg.Address.AddressLine1).NotEmpty();
            RuleFor(reg => reg.Address.Town).NotEmpty();
            RuleFor(reg => reg.Address.Country).NotEmpty();
            RuleFor(reg => reg.Currency.Code).NotEmpty();
            RuleFor(reg => reg.Address.Postcode).NotEmpty();
        }
    }
}
