﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Models
{
    public class MoneyTransfer
    {
        public string SourceAccount { get; set; }
        public string DestinationAccount { get; set; }

        public Money AmountToTransfer { get; set; }
        public decimal ExchangeRateSource { get; set; }
        public decimal ExchangeRateDestination { get; set; }
    }
}
