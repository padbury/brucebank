﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Bank.Core.Models
{
    public class Money
    {
        public Money(decimal amount, string currencyCode)
        {
            Amount = amount;
            CurrencyCode = currencyCode;
        }

        public Money()
        {

        }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }

        public string Formatted
        {
            get
            {
                return string.Format(FindCultureByCurrencyCode(CurrencyCode), "{0:C}", Math.Round(Amount, 2, MidpointRounding.AwayFromZero));
            }
        }

        public string Symbol
        {
            get
            {
                return string.Format(FindCultureByCurrencyCode(CurrencyCode), "{0:C0}", Math.Round(Amount, 0, MidpointRounding.AwayFromZero)).Substring(0, 1);
            }
        }

        public override string ToString()
        {
            return string.Format(FindCultureByCurrencyCode(CurrencyCode), "{0:N0}", Math.Round(Amount, 2, MidpointRounding.AwayFromZero));
        }

        public static CultureInfo FindCultureByCurrencyCode(string currencyCode)
        {

            if (currencyCode.ToLower() == "eur")
            {
                return CultureInfo.CreateSpecificCulture("fr-fr");
            }

            if (currencyCode.ToLower() == "usd")
            {
                return CultureInfo.CreateSpecificCulture("en-US");
            }

            return CultureInfo.CreateSpecificCulture("EN-GB");
        }
    }
}
