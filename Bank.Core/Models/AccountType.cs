﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bank.Core.Models
{
    public class AccountType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
