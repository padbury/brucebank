﻿using Bank.Core.Validation;
using FluentValidation.Results;
using System;

namespace Bank.Core.Models
{
    /// <summary>
    /// The main business object for accounts. It's implements validated entity to force me to add validation.
    /// </summary>
    public class CustomerAccount : IValidatedEntity
    {
        public string Id { get; set; }

        public string SortCode { get; set; }
        public string AccountNumber { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public Address Address { get; set; }

        public Currency Currency { get; set; }

        public string AccountType { get; set; }

        public ValidationResult Validate()
        {
            return new CustomerAccountValidator().Validate(this);
        }

        public bool Equals(CustomerAccount obj)
        {
            return Firstname.ToLower() == obj.Firstname.ToLower() && Lastname.ToLower() == obj.Lastname.ToLower() && Address.AddressLine1.ToLower() == obj.Address.AddressLine1.ToLower()
                && Address.Postcode.ToLower() == obj.Address.Postcode.ToLower();
        }
    }
}
